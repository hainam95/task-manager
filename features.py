import inspect
import os
import sys
import threading
from datetime import datetime

import time
from croniter import croniter

from tasks import *
from constant import CONFIG_DELIMITER
from util import get_config, is_app_running, \
    get_task_queue, update_queue


def add_task(args):
    task_name = args.name
    task_class = args.klass
    task_pattern = args.pattern

    if not task_name or not task_class or not task_pattern:
        print("Error: --name|-n, --class|-c, --pattern|-p required.")
        exit()

    # validate --class
    clsmembers = inspect.getmembers(sys.modules['tasks'], inspect.isclass)
    available_task_classes = [c[0] for c in clsmembers]
    if "Task" in available_task_classes:
        available_task_classes.remove("Task")
    if task_class not in available_task_classes:
        print(f"Error: Class {task_class} is invalid. Choose from: {available_task_classes}")
        exit()

    # Validate --pattern
    base = datetime.now()
    try:
        croniter(task_pattern, base)
    except (ValueError, KeyError) as e:
        print(str(e))
        exit()

    config = f"{task_name.strip()}{CONFIG_DELIMITER}" \
             f"{task_class.strip()}{CONFIG_DELIMITER}" \
             f"{task_pattern.strip()}\n"

    # validate --name
    with open('config', "w+") as f:
        for line in f.readlines():
            if line.split(CONFIG_DELIMITER)[0] == task_name:
                print(f"Error: task '{task_name}' already added to list.")
                exit()

    with open('config', 'a') as f:
        f.write(config)

    print(f"Task '{task_name}' added.")


def start_task(args):
    task_name = args.name
    if not task_name:
        print("Error: --name|-n required.")
        exit()

    configs = get_config()

    if not all([task_name in config.split(CONFIG_DELIMITER)
                for config in configs]):
        print(f"Error: task '{task_name}' not found.")
        exit()

    new_configs = []
    for config in configs:
        if config.startswith("#") and config.find(task_name) > -1:
            new_configs.append(config.replace("#", "").lstrip())
        else:
            new_configs.append(config)

    with open('config', 'w') as f:
        for config in new_configs:
            f.writelines(config)

    print(f"Task {task_name} started.")


def stop_task(args):
    task_name = args.name
    if not task_name:
        print("Error: --name|-n required.")
        exit()

    with open('config', "w+") as f:
        configs = f.readlines()

    new_configs = []
    for config in configs:
        if task_name == config.split("    ")[0]:
            new_configs.append(f"#{config}")
        else:
            new_configs.append(config)

    with open('config', 'w') as f:
        for config in new_configs:
            f.writelines(config)

    print(f"Task {task_name} stopped.")


def pause_task(args):
    stop_task(args)


def remove_task(args):
    task_name = args.name
    if not task_name:
        print("Error: --name|-n required.")
        exit()

    configs = get_config()

    new_configs = []
    for config in configs:
        if task_name != config.split(CONFIG_DELIMITER)[0]:
            new_configs.append(config)

    with open('config', 'w') as f:
        for config in new_configs:
            f.writelines(config)


def export_task_list(args):
    print("Kindly use: 'tm list > some_file', or copy config file directly.")


def import_task_list(args):
    ans = input("You will lose all current config? (y/n) ")
    if ans != 'y':
        return

    file_name = args.file
    if not file_name:
        print("Error: --file|-f required.")
        exit()

    try:
        with open(file_name, "w+") as f:
            new_configs = f.readlines()

    except FileNotFoundError:
        print(f"File not found: {file_name}")
        exit()

    with open('config', 'w') as f:
        for config in new_configs:
            f.writelines(config)

    print("Config imported")


def list_tasks(args):
    configs = get_config()

    if configs:
        for config in configs:
            if not config.startswith("#"):
                print(config)
    else:
        print("No task scheduled.")


def run(args):
    if is_app_running():
        print("Task Manager is already running.")
        exit()

    configs = get_config()
    config_fetch_time = time.time()
    queue = get_task_queue(configs)
    if not queue:
        print("No task added.")
        exit()
    while 1:
        config_edited_time = os.path.getmtime("config")
        if config_edited_time > config_fetch_time:
            config_fetch_time = time.time()
            print("Config reloaded")
            configs = get_config()
            queue = get_task_queue(configs)

        next_execute_task = queue.pop(0)
        c = eval(f"{next_execute_task['class_name']}()")

        next_execute_time = next_execute_task["next_execute_time"]
        print(f"Next task: {next_execute_task['task_name']} in {int(next_execute_time - time.time())}s")
        if time.time() < next_execute_time:
            time.sleep(next_execute_time - time.time())

        update_queue(queue, next_execute_task)
        print(f"Running: {next_execute_task['task_name']}")
        x = threading.Thread(target=c.do_job, args=())
        x.start()
