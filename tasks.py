"""
Tasks should be added here as a subclass of Task.

"""


class Task:
    def do_job(self):
        raise NotImplemented("Task need to implemented do_job")


class NotificationEmail(Task):
    def do_job(self):
        print("Emails sent to users.")


class Counter(Task):
    def do_job(self):
        import time
        print("Task: print from 1 to 10, delay 1s:")
        for i in range(10):
            print(i)
            time.sleep(1)


class UpdateStatistic(Task):
    statistic_name = "inactive_account"

    def do_job(self):
        # This job can use data of another job if needed.
        clear_inactive_accounts_task = ClearInactiveAccounts()
        clear_inactive_accounts_task.do_job()
        number_of_inactive_account = clear_inactive_accounts_task.number_of_inactive_account
        print(f"Statistic updated: Number of inactive account: {number_of_inactive_account}")


class ClearInactiveAccounts(Task):
    number_of_inactive_account = 10

    def do_job(self):
        self.number_of_inactive_account += 10
        print("Inactive account cleared.")
