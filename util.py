import os

import psutil
from croniter import croniter

from constant import CONFIG_DELIMITER


def get_config():
    if not os.path.isfile('config'):
        open('config', 'w+')
    with open('config', 'r+') as f:
        configs = f.readlines()
    return [config for config in configs if not config.startswith("#")]


def is_app_running():
    for q in psutil.process_iter():
        if q.name().startswith('python'):
            if len(q.cmdline()) > 1 and __file__ in q.cmdline()[1] and q.pid != os.getpid():
                return True
    return False


def insert_task_to_queue(queue, job):
    queue.append(job)
    return queue


def get_next_execute_task(configs):
    return get_task_queue(configs)[0]


def get_last_execute_task(configs):
    return get_task_queue(configs)[-1]


def get_task_queue(configs):
    queue = []

    for config in configs:
        pattern = config.split(CONFIG_DELIMITER)[-1]
        next_execute_time = croniter(pattern).get_next()
        class_name = config.split(CONFIG_DELIMITER)[1]
        task_name = config.split(CONFIG_DELIMITER)[0]

        queue.append({
            "task_name": task_name,
            "class_name": class_name,
            "next_execute_time": next_execute_time,
            "pattern": pattern,
        })
    queue.sort(key=lambda x: x["next_execute_time"])
    return queue


def update_queue(queue, last_run_task):
    pattern = last_run_task["pattern"]
    next_execute_time = croniter(pattern).get_next()
    last_run_task["next_execute_time"] = next_execute_time
    queue.append(last_run_task)
    queue.sort(key=lambda x: x["next_execute_time"])
    return queue
