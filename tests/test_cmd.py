import argparse
import unittest
import os.path
import importlib
from unittest.mock import patch

from features import add_task, start_task, run, stop_task, pause_task, remove_task, import_task_list, list_tasks
import tasks

test_tasks = """
class Task:
    def do_job(self):
        raise NotImplemented("Task need to implemented do_job")


class TestTask1(Task):
    def do_job(self):
        print("Test task 1 complete.")
"""


class TestCmd(unittest.TestCase):
    def setUp(self) -> None:
        self.parser = argparse.ArgumentParser()

        actions = ["add", "start", "stop", "pause",
                   "remove", "export", "import", "list", "run"]

        self.parser.add_argument('action', choices=actions)

        self.parser.add_argument('-n', '--name', help='name of the task.', metavar='')
        self.parser.add_argument('-c', '--class', dest="klass", metavar='')
        self.parser.add_argument('-p', '--pattern', metavar='')

        self.parser.add_argument('-f', '--file', metavar='')

        # Backup task file.
        with open(f'{os.path.dirname(__file__)}/../tasks.py', 'r+') as f:
            self.tasks = f.readlines()
            f.seek(0)
            f.write(test_tasks)
            f.truncate()

        importlib.reload(tasks)

    def tearDown(self) -> None:
        # Revert tasks.py
        with open(f'{os.path.dirname(__file__)}/../tasks.py', 'r+') as f:
            f.seek(0)
            f.writelines(self.tasks)
            f.truncate()
        os.remove('config')

    def test_add_task(self):
        args = self.parser.parse_args(['add', '-n', 'Task 1', '-c', 'NotificationEmail', '-p', '* * * * * *'])
        add_task(args)

    def test_start_task(self):
        args = self.parser.parse_args(['start', '-n', 'Task 1'])
        start_task(args)

    def test_stop_task(self):
        args = self.parser.parse_args(['stop', '-n', 'Task 1'])
        stop_task(args)

    def test_pause_task(self):
        args = self.parser.parse_args(['pause', '-n', 'Task 1'])
        pause_task(args)

    def test_remove_task(self):
        args = self.parser.parse_args(['remove', '-n', 'Task 1'])
        remove_task(args)

    @patch('builtins.input', return_value='y')
    def test_import_task_list(self, input):
        args = self.parser.parse_args(['import', '-f', 'config'])
        import_task_list(args)

    def test_list_tasks(self):
        args = self.parser.parse_args(['remove', '-n', 'Task 1'])
        list_tasks(args)

    # This test would run forever
    # def test_run(self):
    #     args = self.parser.parse_args(['run'])
    #     run(args)


if __name__ == '__main__':
    unittest.main()
