### Prerequisites:
#### python3
#### Libraries
Install with: *pip install -r requirements.txt*

- croniter: Reading and parsing cron-like patterns.
- psutil: Manage processes.

## Task Manager
Task Manager is a python tool for managing tasks.

User provide task definitions on tasks.py file. Then add tasks to queue and run it.

#### Quick usage:
- Implement class Task on tasks.py file, eg: SendEmail
- Add tak to queue to run every day at 00:00: python tm.py add -n "my first task" -c "SendEmail" -p "0 0 * * *"
- Start the tool: python tm.py run

## Document
### Usage 
*python tm.py <action\> <options\>*

action is one of:
add, start, stop, pause, remove, export, import, list, run

#### Add new task
A task can be added to the queue with command:

*python tm.py add -n <task-name\> -c <class-name\> -p <cron-like-pattern\>*

<task-name\>: Name of task, which will be used to manage task.

<class-name\>: Name of class which will corresponding to run the task. 
class-name is defined in tasks.py file. This class have to implemented from Task class, which also be in tasks.py file.


<cron-like-pattern\>: A cron-like pattern to determine the repeatition of the task. 
For example: * * * * * * : Run every minute.

#### Stop a task
*python tm.py stop -n <task-name\> *

<task-name\>: Name of task to stop. 

This command will put a # on config file, so it won't be run in the future.

#### Start a task
*python tm.py start -n <task-name\> *

<task-name\>: Name of task to start. This command will remove # of task on config file if any.

#### Pause a task
*python tm.py pause -n <task-name\> *

Similar to stop a task.

In the future, this action will record time to next run and continue from that time when start again.

#### Remove a task
*python tm.py start -n <task-name\> *

<task-name\>: Similar to stop a task. 

#### Export tasks
*python tm.py export*

When user want to run on another machine, just need to copy tasks.py and config file to tm folder.

#### Import tasks
*python tm.py import -f <file-name\>*

<file-name\>: File need to import.

Import task from a file. This will clear the current config.

#### Display tasks queue
*python tm.py list*

Show tasks in the queue.

#### Run the tool
*python tm.py run*

---------

### How it works:

Tasks are stored as classes in tasks.py file. Config for tasks are store in config file.
While started, the tool will read the config file to get list of task which need to run and 
the repetition for that task. 
While running tasks, firstly, it creates a queue of task from config file, in which the soonest task will be on the top
of the queue, then it sleeps until running the nearest task. After run that task, it put the task back on the queue 
in a proper position.
It repeats that until receive kill signal.

For running on another devide, just need to copy config and classes file.

### Test
*python -m unittest*

Test will backup current config and tasks, then revert them after.

### Graceful shutdown
Using python thread to run tasks, so task will run till the end no matter main process is running or not.
Can test with the counter task. When send kill signal, it's still counting.