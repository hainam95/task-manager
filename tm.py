#!/usr/bin/python

import argparse

from features import *

parser = argparse.ArgumentParser(description='Manage tasks.')

actions = ["add", "start", "stop", "pause",
           "remove", "export", "import", "list", "run"]

parser.add_argument('action', choices=actions,
                    help='An action to do with the task.')

parser.add_argument('-n', '--name', help='name of the task.', metavar='')
parser.add_argument('-c', '--class', dest="klass", metavar='',
                    help='responsible class which run the task.'
                         ' This class should implement Task interface.')
parser.add_argument('-p', '--pattern', metavar='',
                    help='cron-like pattern which determine the repetition of the task.'
                         ' Easily create one at: https://crontab.guru/', )

parser.add_argument('-f', '--file', metavar='',
                    help='File name when importing tasks.')

actions_mapping = {
    "add": add_task,
    "start": start_task,
    "stop": stop_task,
    "pause": pause_task,
    "remove": remove_task,
    "export": export_task_list,
    "import": import_task_list,
    "list": list_tasks,
    "run": run,
}

if __name__ == "__main__":
    args = parser.parse_args()
    action = actions_mapping.get(args.action)
    if action:
        action(args)
